
## What?

<Describe the problem we want to address>

## Why?

<Why do we need to study this topic>

## How?

1. Create the new page under https://gitlab.e.foundation/internal/wiki/-/wikis/studies
2. and add the link at https://gitlab.e.foundation/internal/wiki/-/wikis/studies
3. Fill the template below, and add it on the page you created previously
4. Discuss the **recommendation** with at least tech lead and manager, before aligning on the **choice**

```markdown
# Study

## 1. The problem to solve

<!-- Describe your understanding of the problem. Add any existing constraint (time, technology, budget...) to solve this topic.  -->

## 2. The approaches

<!-- Explain each approach technically, share pro and cons, and also give an idea of the development time, and the required knowledge  -->

## 3. The recommendation

<!-- Share which approach you would recommend, and why -->

## 4. The choice

<!-- Given the chosen approach, and the reasons behind (to be filled after a meeting) -->
```


/cc @jonathanklee @rhunault
/assign 
/label ~"type::Study" 
/due
