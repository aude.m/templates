* [ ] Validation of OTA upgrade<beta milestone>
* [ ] Confirm previous Android version should not have been published
    - it should have been removed from https://gitlab.e.foundation/e/os/releases
* [ ] Documentation (upgrade + point to the last Android version)
* [ ] Open download
* [ ] Share with communication team for communication 

/label ~"type::Task"
/milestone %
