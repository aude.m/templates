- project name: 
- upstream url:
- target group:

## TODO

- [ ] Fork the project
- [ ] [Configure the project](https://gitlab.e.foundation/internal/wiki/-/wikis/source-code/how-to-fork-a-project)
- [ ] Create /e/ branches
    - `v1-nougat`, `v1-oreo`, `v1-pie`, `v1-q` on android version dependant projects
- [ ] Setup the project in [/e/ android manifest](https://gitlab.e.foundation/e/os/android) for the build system
- [ ] [Setup synchronisation with upstream](https://gitlab.e.foundation/internal/wiki/-/wikis/source-code/how-to-setup-automatic-sync-with-upstream)
  - Ask admin to set an admin as owner of the scheduled jobs
  - Don't forget to add the new project in [sync with upstream dashboard](https://gitlab.e.foundation/internal/wiki/-/wikis/source-code/sync-with-upstream-dashboard)

/confidential 
/label ~"type::Task"
